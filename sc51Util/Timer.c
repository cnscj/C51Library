#include "Timer.h"
#include "Calculate.h"
#include <reg51.h>
timerfunc g_timer_func[CALLBACKFUNCNUM] = {0};
///////////////
int TimerConfig(char tmod,char thx,char tlx)
{
	//可从定时器工作模式中判断所使用的定时器
	TMOD = tmod;     //定时器工作模式

	 if(tmod)
	 {
		 if( (tmod) & 0x0f)
		 {
		 	TH0 = thx?thx:TH0;
			TL0 = tlx?thx:TL0;
			TR0 = 1;
		 }
		 if((tmod >> 4)& 0x0f) 
		 {
			TH1 = thx?thx:TH1;
			TL1 = tlx?thx:TL1;
			TR1 = 1;
		 }
	 }
	 return 1;
}

int TimerInit(Timer *ptr,uchar timer,timerfunc func,double interval)
{
	if(!ptr)return 0;

	ptr->timer = timer;
	ptr->func = func;
	ptr->interval = interval;

	return 1;
}

void TimerStart(Timer *ptr)
{
	char tmod,x,thx,tlx;
	x=ptr->timer;
	CalculateTimer(DEFCRYFRE,ptr->interval,&thx,&tlx);
	tmod=x?0x10:0x01;
	g_timer_func[x] = ptr->func;
 	TimerConfig(tmod,thx,tlx);		//设置定时器时间
	TimerConfig(0x11,NULL,NULL);	//设置定时器工作模式
/*	switch(x)
	{	case 0:
			TR0 = 1;
			break;
		case 1:
			TR1 = 1;
			break;
	}
	*/
}

void TimerStop(Timer *ptr)
{
	char x=ptr->timer;	
	switch(x)
	{	case 0:
			TR0 = 0;
			break;
		case 1:
			TR1 = 0;
			break;
	}
	g_timer_func[x] = NULL;	
}



#define TIMER0_FUNC(__FUNC__) \
void TIMER0_INTERRUPT_FUNC() TIMER0_INTE \
{ \
	__FUNC__; \
}


#define TIMER1_FUNC(__FUNC__) \
void TIMER1_INTERRUPT_FUNC() TIMER1_INTE \
{ \
	__FUNC__; \
}


#ifndef CLOSE_TIMER0
TIMER0_FUNC(g_timer_func[0]())
#endif

#ifndef CLOSE_TIMER1
TIMER1_FUNC(g_timer_func[1]())
#endif