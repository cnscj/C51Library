#ifndef __PWM_H_
#define __PWM_H_
#include "Interrupt.h"
typedef void (*pwmfunc)(char);	//pwm回调函数

extern uint g_pwm_speed;			//pwm速度
extern uint g_pwm_count;			//pwm计数器
extern pwmfunc g_pwm_func;
/*----------------------------------------------------
 函数名：	PWMInit()
 功能：		PWM脉冲初始化
 参数:		[in]cycle 周期 单位ms
 			[in]func 回调函数
 返回值:	结果
------------------------------------------------------*/
int PWMInit(double cycle,pwmfunc func);

/*----------------------------------------------------
 函数名：	PWMStart()
 功能：		PWM脉冲启动
 参数:		[in]time 周期 单位ms/r
 返回值:	无
------------------------------------------------------*/
void PWMStart(uint time);

/*----------------------------------------------------
 函数名：	PWMStep()
 功能：		PWM脉冲一次执行
 参数:		无
 返回值:	无
------------------------------------------------------*/
void PWMStep();

/*----------------------------------------------------
 函数名：	PWMStop()
 功能：		PWM脉冲停止
 参数:		无
 返回值:	无
------------------------------------------------------*/
void PWMStop();


#endif