#ifndef __CALCULATE_H_
#define __CALCULATE_H_
#include "..\sc51Core\Macros.h"
/*----------------------------------------------------
 函数名：	CalculateBaudRate()
 功能：		计算波特率
 参数:		[in]cryfre 晶振频率 MHZ
 			[in]baud 波特率
 			[out]*th 计数器x的高8位寄存器
 			[out]*tl 计数器x的低8位寄存器
 返回值:	结果
------------------------------------------------------*/
int CalculateBaudRate(double cryfre,uint baud,char *th,char *tl);

/*----------------------------------------------------
 函数名：	CalculateTimer()
 功能：		计算定时器
 参数:		[in]cryfre 晶振频率 MHZ
 			[in]interval 间隔时间
 			[out]*th 计数器x的高8位寄存器
 			[out]*tl 计数器x的低8位寄存器
 返回值:	结果
------------------------------------------------------*/
int CalculateTimer(double cryfre,double interval,char *th,char *tl);

#endif