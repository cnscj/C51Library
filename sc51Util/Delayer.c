#include "Delayer.h"
#include <intrins.h>

void DelayerUs(uint time)
{
	uint iCount;
	for(iCount = 0;iCount< time ; iCount++)
	{
		//以下代码适用于STC90C系列
		#ifdef STC90CXX
		{
						//误差巨大,大概5us
		}
		#endif
	
		//以下代码适用于STC12C系列
		#ifdef STC12CXX
		{
			_nop_();
		}
		#endif
	}	
}

void DelayerMs(uint time)
{
	uint iCount;uchar i, j;
	for(iCount = 0;iCount< time ; iCount++)
	{
		//以下代码适用于STC90C系列
		#ifdef STC90CXX
		{
			_nop_();
			i = 2;
			j = 199;
			do{
				while (--j);
			}while (--i);
		}
		#endif
	
		//以下代码适用于STC12C系列
		#ifdef STC12CXX
		{
			_nop_();
			i = 11;
			j = 190;
			do{
				while (--j);
			}while (--i);
		}
		#endif
	}
}

void DelayerStep(void (*func)(uint),uint time)
{
	func(time);	
}