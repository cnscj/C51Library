#include "Buffer.h"
int BufferInit(Buffer *ptr)
{
	ptr->front=ptr->back=BUFFERSIZE-1;
	return 1;
}


int BufferInsert(Buffer *ptr,int pos,BUFFERDATA *buf,int size)
{
	int iCount,jCount,nTotal;
	nTotal = BufferLength(ptr);
	if(nTotal+size >= BUFFERSIZE) return 0;
	if(nTotal<pos) return 0;
	else {
		//将整条数据先前移动size个单位,
		
		for(iCount=0;iCount<size;iCount++)
		{
			for(jCount=0;jCount<nTotal-pos;jCount++)
			{
				ptr->dat[(ptr->back + iCount - jCount + 1)%BUFFERSIZE] = ptr->dat[(ptr->back + iCount - jCount)%BUFFERSIZE];
			}
			ptr->dat[(ptr->front + 1 + pos + iCount)%BUFFERSIZE]=buf[iCount];
		}
		ptr->back = (ptr->back+size)%BUFFERSIZE;
		return 1;
	}

}


int BufferFetch(Buffer *ptr,int pos,BUFFERDATA *buf,int size)
{
	int iCount,jCount,nTotal;
	nTotal = BufferLength(ptr);
	if(size>nTotal) return 0;
	if(nTotal<pos) return 0;
	else {
		//取出数据,并让数组后移size个单位
		for(iCount=0;iCount<size;iCount++)
			buf[iCount]=ptr->dat[(ptr->front + 1 + pos + iCount)%BUFFERSIZE];
		for(iCount=0;iCount<size;iCount++)
		{
			for(jCount=0;jCount<nTotal-pos;jCount++)
			{
				ptr->dat[(ptr->front + 1 + pos - iCount + jCount  - 1 )%BUFFERSIZE] =ptr->dat[(ptr->front + 1 + pos - iCount + jCount )%BUFFERSIZE];
			}
			
		}
		ptr->back = ptr->back-size<0 ? BUFFERSIZE+(ptr->back-size) : ptr->back-size;
		return 1;
	}
}


int BufferPush(Buffer *ptr,BUFFERDATA dat)
{
	if(BufferIsFull(ptr)) return 0;
	else {
		ptr->back=(ptr->back+1)%BUFFERSIZE;
		ptr->dat[ptr->back]=dat;
		return 1;
	}
}

BUFFERDATA BufferFront(Buffer *ptr)
{
	return ptr->dat[(ptr->front+1)%BUFFERSIZE];
}

BUFFERDATA BufferBack(Buffer *ptr)
{
	return ptr->dat[ptr->back];
}

void BufferPopBack(Buffer *ptr)
{
	if(BufferIsEmpty(ptr)) return ;
	else{
		ptr->back = ptr->back-1<0 ? BUFFERSIZE-1 : ptr->back-1;
	}
}

void BufferPopFront(Buffer *ptr)
{
	if(BufferIsEmpty(ptr)) return ;
	else{ 
		ptr->front = (ptr->front+1)%BUFFERSIZE;
	}
}

int BufferIsEmpty(Buffer *ptr)
{
	if(ptr->front==ptr->back)	return 1;
	else return 0;
}

int BufferIsFull(Buffer *ptr)
{
	//if(BufferLength(ptr) >= BUFFERSIZE) return 1;
	if(ptr->front == (ptr->back+1)%BUFFERSIZE) return 1;
	else return 0;
}

void BufferClear(Buffer *ptr)
{
	BufferInit(ptr);
}

uint BufferLength(Buffer *ptr)
{
	if (ptr->back >= ptr->front) return ptr->back - ptr->front;
	else return ptr->back + BUFFERSIZE - ptr->front;
}