#ifndef __GPIO_H_
#define __GPIO_H_
#include "..\sc51Core\Macros.h"
#include "Interrupt.h"

//51单片机特性,无法通过函数来设置或获取GPIO口状态

/*----------------------------------------------------
 函数名：	GPIOScan()
 功能：		扫描io口
 参数:		[in]io IO口
 返回值:	结果
------------------------------------------------------*/
char GPIOScan(char io);


/*----------------------------------------------------
 函数名：	GPIOCheck()
 功能：		检测io口状态
 参数:		[in]io IO口
 			[in]state 状态
 返回值:	结果
------------------------------------------------------*/
char GPIOCheck(char io,char state);

#endif