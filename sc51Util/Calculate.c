#include "Calculate.h"

int CalculateBaudRate(double cryfre,uint baud,char *th,char *tl)
{
	if( !th || !tl) return 0;

	*th=256-cryfre*1000000/(baud*32*12);	 
	*tl=*th;
	return 1;
}

int CalculateTimer(double cryfre,double interval,char *th,char *tl)
{
	float period;
	if( !th || !tl ) return 0;
	period = 1 / (cryfre / 12);
	*th=(uint)((65536-(interval * 1000))*period)/256;
	*tl=(uint)((65536-(interval * 1000))*period)%256;

	return 1;
}