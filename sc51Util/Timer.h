#ifndef __TIMER_H_
#define __TIMER_H_
#include "..\sc51Core\Macros.h"
#include "Interrupt.h"

#define CALLBACKFUNCNUM 2
//回调函数
typedef void (*timerfunc)();
extern timerfunc g_timer_func[CALLBACKFUNCNUM];

//定时器设置
typedef struct tagTimer
{
	uchar timer;			//所使用定时器
	timerfunc func;			//回调函数
	double interval;		//时间间隔
}Timer;

/*----------------------------------------------------
 函数名：	TimerConfig()
 功能：		全局定时器配置
 参数:		[in]tmod 定时器工作方式
 			[in]thx 计数器x的高8位寄存器  , 为 0 时不改变
			[in]tlx 计数器x的低8位寄存器  , 为 0 时不改变

 返回值:	结果
------------------------------------------------------*/
int TimerConfig(char tmod,char thx,char tlx);

/*----------------------------------------------------
 函数名：	TimerInit()
 功能：		定时器初始化
 参数:		[out]ptr 对象地址
 			[in]timer	定时器x
			[in]timerfunc 回调函数
			[in]interval 延迟时间 单位ms
 返回值:	结果
------------------------------------------------------*/
int TimerInit(Timer *ptr,uchar timer,timerfunc func,double interval);

/*----------------------------------------------------
 函数名：	TimerStart()
 功能：		开启定时器
 参数:		[in]ptr 对象地址
 返回值:	无
------------------------------------------------------*/
void TimerStart(Timer *ptr);

/*----------------------------------------------------
 函数名：	TimerStop()
 功能：		停止定时器
 参数:		[in]ptr 对象地址
 返回值:	无
------------------------------------------------------*/
void TimerStop(Timer *ptr);


#endif