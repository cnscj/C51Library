#include "Fat32.h"
#include <string.h>
#include <ctype.h>

unsigned char * FAT32_ReadSector(unsigned long LBA,unsigned char *buf) //FAT32中读取扇区的函数
{
 MMC_get_data_LBA(LBA,512,buf);
 return buf;
}

unsigned char FAT32_WriteSector(unsigned long LBA,unsigned char *buf)//FAT32中写扇区的函数
{
 return MMC_write_sector(LBA,buf);
}

unsigned long lb2bb(unsigned char *dat,unsigned char len) //小端转为大端
{
 unsigned long temp=0;
 unsigned long fact=1;
 unsigned char i=0;
 for(i=0;i<len;i++)
 {
  temp+=dat[i]*fact;
  fact*=256;
 }
 return temp;
}

unsigned long  FAT32_FindBPB()  //寻找BPB所在的扇区号
{
 
 unsigned long Range=0;
 while(Range<FIND_BPB_UP_RANGE)
 { 
  FAT32_ReadSector(Range,FAT32_Buffer);
  if(FAT32_Buffer[0]==0xeb && FAT32_Buffer[2]==0x90)
   break;
  Range++;
 }
 return Range;
 /*
 FAT32_ReadSector(0,FAT32_Buffer);
 return lb2bb(((((struct PartSector *)(FAT32_Buffer))->Part[0]).StartLBA),4);
 */
}

unsigned long FAT32_Get_Total_Size() //存储器的总容量，单位为M
{
 FAT32_ReadSector(Init_Arg.BPB_Sector_No,FAT32_Buffer);
 return ((float)(lb2bb((((struct FAT32_BPB *)(FAT32_Buffer))->BPB_TotSec32),4)))*0.0004883;
}

void FAT32_Init(struct FAT32_Init_Arg *arg)
{

 struct FAT32_BPB *bpb=(struct FAT32_BPB *)(FAT32_Buffer);             	  //将数据缓冲区指针转为struct FAT32_BPB 型指针
 arg->BPB_Sector_No   =FAT32_FindBPB();                                   //FAT32_FindBPB()可以返回BPB所在的扇区号
 arg->Total_Size      =FAT32_Get_Total_Size();                            //FAT32_Get_Total_Size()可以返回磁盘的总容量，单位是兆
 arg->FATsectors      =lb2bb((bpb->BPB_FATSz32)    ,4);                   //装入FAT表占用的扇区数到FATsectors中
 arg->FirstDirClust   =lb2bb((bpb->BPB_RootClus)   ,4);                   //装入根目录簇号到FirstDirClust中
 arg->BytesPerSector  =lb2bb((bpb->BPB_BytesPerSec),2);                   //装入每扇区字节数到BytesPerSector中
 arg->SectorsPerClust =lb2bb((bpb->BPB_SecPerClus) ,1);                   //装入每簇扇区数到SectorsPerClust 中
 arg->FirstFATSector  =lb2bb((bpb->BPB_RsvdSecCnt) ,2)+arg->BPB_Sector_No;//装入第一个FAT表扇区号到FirstFATSector 中
 arg->RootDirCount    =lb2bb((bpb->BPB_RootEntCnt) ,2);                   //装入根目录项数到RootDirCount中
 arg->RootDirSectors  =(arg->RootDirCount)*32>>9;                         //装入根目录占用的扇区数到RootDirSectors中
 arg->FirstDirSector  =(arg->FirstFATSector)+(bpb->BPB_NumFATs[0])*(arg->FATsectors); //装入第一个目录扇区到FirstDirSector中
 arg->FirstDataSector =(arg->FirstDirSector)+(arg->RootDirSectors); //装入第一个数据扇区到FirstDataSector中


}
 

void strnupr(char *str,int n)
{
	int i;
	for(i=0;i<n && str[i];i++)
	{
		if(str[i]>='a' && str[i]<='z')
			str[i]-='a'-'A';
	}
}


unsigned long FAT32_GetNextCluster(unsigned long LastCluster)
{
 unsigned long temp;
 struct FAT32_FAT *pFAT;
 struct FAT32_FAT_Item *pFAT_Item;
 temp=((LastCluster/128)+Init_Arg.FirstFATSector);
 FAT32_ReadSector(temp,FAT32_Buffer);
 pFAT=(struct FAT32_FAT *)FAT32_Buffer;
 pFAT_Item=&((pFAT->Items)[LastCluster%128]);
 return lb2bb((unsigned char *)pFAT_Item,4);
}



struct FileInfoStruct *SDEToFileInfo(struct FileInfoStruct *pFileInfo,struct FAT32_ShortDirEntry *pDirEntry)
{
	pFileInfo->FileSize=lb2bb(pDirEntry->deFileSize,4);
	strncpy(pFileInfo->FileName,pDirEntry->deName,8);pFileInfo->FileName[8]='.';
	strncpy(pFileInfo->FileName+9,pDirEntry->deExtension,3);pFileInfo->FileName[12]='\0';
	pFileInfo->FileStartCluster=lb2bb(pDirEntry->deLowCluster,2)+lb2bb(pDirEntry->deHighClust,2)*65536;
	pFileInfo->FileCurCluster=pFileInfo->FileStartCluster;
	pFileInfo->FileNextCluster=FAT32_GetNextCluster(pFileInfo->FileCurCluster);
	pFileInfo->FileOffset=0;
	return pFileInfo;
}

struct FileReaderStruct *SDEToReaderInfo(struct FileReaderStruct *pReaderInfo,struct FAT32_ShortDirEntry *pDirEntry)
{
	pReaderInfo->FileTotalSize=lb2bb(pDirEntry->deFileSize,4);
	pReaderInfo->ReaderStartCluster=lb2bb(pDirEntry->deLowCluster,2)+lb2bb(pDirEntry->deHighClust,2)*65536;

	pReaderInfo->ReaderCurCluster=pReaderInfo->ReaderStartCluster;
	pReaderInfo->ReaderNextCluster=FAT32_GetNextCluster(pReaderInfo->ReaderCurCluster);

	pReaderInfo->HadReadSize=0;
	pReaderInfo->ReceiveSize=0;
	pReaderInfo->ReaderTempData=0;

	pReaderInfo->ReaderBufferCur=0;
	pReaderInfo->UserBufferCur=0; 
	
	pReaderInfo->iSectorInCluster=0;
	pReaderInfo->ReaderFileIsOpen=1;

	return pReaderInfo;
}

struct FAT32_ShortDirEntry *FAT32_FindFileByDir(struct FAT32_ShortDirEntry *pDirEntry,char *pszName)
{
	unsigned long StartCluster;
	unsigned long iStartDirSector,iCurSector;
	unsigned long iDirCluster;

	unsigned long iDir;
	struct FAT32_ShortDirEntry *pResultDir;

	if(pDirEntry==0)StartCluster=2;		//初次进入时必须令iStartDirSector=Init_Arg.FirstDirSector;
	else StartCluster=lb2bb(pDirEntry->deLowCluster,2)+lb2bb(pDirEntry->deHighClust,2)*65536;

	for(iDirCluster=StartCluster;
		iDirCluster!=0x0fffffff;
		iDirCluster=FAT32_GetNextCluster(iDirCluster))	  //读取下一个簇的内容
	{	
		iStartDirSector=(Init_Arg.FirstDirSector)+(iDirCluster-2)*(Init_Arg.SectorsPerClust);
		for(iCurSector=iStartDirSector;iCurSector<iStartDirSector+(Init_Arg.SectorsPerClust);iCurSector++)	  //读取一个扇区的内容
		{	
			//第 iCurDirSector 扇区数据,512字节
			FAT32_ReadSector(iCurSector,FAT32_Buffer);
			for(iDir=0;iDir<Init_Arg.BytesPerSector;iDir+=sizeof(struct FAT32_ShortDirEntry))
			{
						 
				pResultDir=((struct FAT32_ShortDirEntry *)(FAT32_Buffer+iDir));
				if((pResultDir->deName)[0]!=0x00 /*无效目录项*/ && (pResultDir->deName)[0]!=0xe5 /*无效目录项*/ && (pResultDir->deName)[0]!=0x0f /*无效属性*/)
				{
					//目录条目匹配必须通过13位进行匹配
				//	if(strncmp(pDirEntry->deName,pResultDir->deName,8)==0 && strncmp(pDirEntry->deExtension,pResultDir->deExtension,3)==0)
					if(strncmp(pszName,pResultDir->deName,8)==0 && strncmp(pszName+8,pResultDir->deExtension,3)==0)
					{	
						
						return pResultDir;
									
					}
					
				}
			}
		}
	}
	return 0;
}

struct FileReaderStruct	*FAT32_Open(char *filepath)
{
	struct FAT32_ShortDirEntry *pResult=0;
	char drname[12];
	int itera=0;int icopy=0;

	for(itera;filepath[itera];itera++)
	{
		if(filepath[itera]=='\\')
		{
			//目录
			if(icopy !=0)
			{
				
				strnupr(drname,12);
				
				//ToDo:
				pResult=FAT32_FindFileByDir(pResult,drname);
				if(pResult==0)return 0;

			} else if(itera !=0 )return 0;
			icopy=0;
			memset(drname,0x20,11);drname[11]='\0';
			continue;
		}else if(filepath[itera]=='.')//有弊端,若中间有'.'怎么算
		{
			icopy=8;
			continue;
		}else{
			drname[icopy++]=filepath[itera];
		}
	}
	//文件
	{
		strnupr(drname,12);
		//ToDo:
		pResult=FAT32_FindFileByDir(pResult,drname);
		if(pResult==0)return 0;
	}
	{
		//构建访问项目
		//ToDo:
		SDEToReaderInfo(&FileReader,pResult);
		
		return &FileReader;
	}
	return 0;

}

unsigned char eof(struct FileReaderStruct *pReader)
{
	if(pReader->FileTotalSize-pReader->ReceiveSize==0)return 1;
	else return 0;
}


//返回读取的大小
void FAT32_Read512Data(struct FileReaderStruct *pReader,char *buffer)
{
 unsigned long Sub;
   
 if(pReader->ReaderNextCluster!=0x0fffffff)
 {
	if(pReader->iSectorInCluster+1 >= Init_Arg.SectorsPerClust){
		Sub=pReader->ReaderCurCluster;//保存当前位置
		pReader->ReaderCurCluster = pReader->ReaderNextCluster; 
		pReader->ReaderNextCluster=FAT32_GetNextCluster(pReader->ReaderCurCluster);//会对buffer内容进行修改

		FAT32_ReadSector((((Sub)-2)*(Init_Arg.SectorsPerClust))+Init_Arg.FirstDataSector+(pReader->iSectorInCluster),buffer);
		pReader->HadReadSize+=Init_Arg.BytesPerSector;

		pReader->iSectorInCluster=0;
	}else{
		FAT32_ReadSector((((pReader->ReaderCurCluster)-2)*(Init_Arg.SectorsPerClust))+Init_Arg.FirstDataSector+(pReader->iSectorInCluster++),buffer);
		pReader->HadReadSize+=Init_Arg.BytesPerSector;

	}	
 }
 else 
 { 
 	 Sub=(pReader->FileTotalSize) - (pReader->HadReadSize);
	 if(Sub >= Init_Arg.BytesPerSector)
	 {
	 
		FAT32_ReadSector((((pReader->ReaderCurCluster)-2)*(Init_Arg.SectorsPerClust))+Init_Arg.FirstDataSector+(pReader->iSectorInCluster++),buffer);
		pReader->HadReadSize+=Init_Arg.BytesPerSector;
	
	 }
	 else 
	 {
		FAT32_ReadSector((((pReader->ReaderCurCluster)-2)*(Init_Arg.SectorsPerClust))+Init_Arg.FirstDataSector+(pReader->iSectorInCluster),buffer); //读取最后一个扇区
		pReader->HadReadSize+=Sub;
	
	 }
 }
}

unsigned char FAT32_ReadNextData(struct FileReaderStruct *pReader)
{
	FAT32_Read512Data(pReader,FAT32_Buffer);
	return FAT32_Buffer;
}

//读完一次512或buffer满就返回,不管buffer是否读完
int FAT32_Read(struct FileReaderStruct *pReader,char *pBuffer,int nBuffersize)
{	
	int i;int nNeedRead=nBuffersize;
	unsigned long Sub;
	if(pReader->ReceiveSize == pReader->FileTotalSize) return -1;

	 pReader->UserBufferCur=0;

	//表示新开始读取或已经读完一个512,切换到下一个
	if(pReader->ReceiveSize ==0 || pReader->ReaderBufferCur >= Init_Arg.BytesPerSector){
		  FAT32_ReadNextData(pReader);
		  pReader->ReaderBufferCur=0;
	}
	
	Sub=pReader->FileTotalSize - pReader->ReceiveSize;
	if(Sub >=Init_Arg.BytesPerSector)nNeedRead=Init_Arg.BytesPerSector;
	else nNeedRead=Sub-pReader->ReaderBufferCur;

	for(i=0;i<nBuffersize;i++)
	{ 
	  if(pReader->ReaderBufferCur>nNeedRead)return i;
	  pBuffer[i]=FAT32_Buffer[pReader->ReaderBufferCur];
	  pReader->ReceiveSize++;
	  pReader->ReaderBufferCur++;
	  pReader->UserBufferCur++;
	}

	return i;

	
}
//读完一次buffer就返回,不管512是否读完(需要考虑大于512的情况)
int FAT32_ReadFull(struct FileReaderStruct *pReader,char *pBuffer,int nBuffersize)
{
	int i;
	if(pReader->ReceiveSize == pReader->FileTotalSize)return -1;

	//需要多次读取
	for(i=0;i<nBuffersize;i++)
	{	
		if(pReader->ReceiveSize == pReader->FileTotalSize)return i;
		//新读取或如果已经读完一次512,切换到下一个512
		if(pReader->ReceiveSize ==0|| pReader->ReaderBufferCur >= Init_Arg.BytesPerSector)
		{
			FAT32_ReadNextData(pReader);
			pReader->ReaderBufferCur=0;
		}
		
  		pBuffer[i]=FAT32_Buffer[pReader->ReaderBufferCur];
  		pReader->ReceiveSize++;
  		pReader->ReaderBufferCur++;
 	
	}
	return i;

}