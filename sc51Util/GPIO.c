#include "GPIO.h"
#include "Delayer.h"
#include <reg51.h>
char GPIOScan(char io)
{
	uchar keyValue = 0 , i; //保存键值

	//--检测按键1--//
	if (io != 0xFF)		//检测按键K1是否按下
	{
		DelayerMs(1);	//消除抖动

		if (io != 0xFF)	//再次检测按键是否按下
		{
			keyValue = io;
			i = 0;
			while ((i<50) && (io != 0xFF))	 //检测按键是否松开
			{
				DelayerMs(1);
				i++;
			}
		}
	}

	return keyValue;   //将读取到键值的值返回
}
char GPIOCheck(char io,char state)
{
	return io == state?HIGH:LOW;
}



//打开GPIO中断
#define GPIO_FUNC(__FUNC__) \
void GPIO_INTERRUPT_FUNC() EX0_INTE	 \
{  \
   __FUNC__;\
}
