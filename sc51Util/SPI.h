#ifndef __SPI_H_
#define __SPI_H_
#include "GPIO.h"
/**************************************************************************************
//------------------ MMC/SD-Card Reading and Writing implementation -------------------
//FileName     : mmc.c
//Function     : Connect STC51 to MMC/SD 
//Created by   : ZHENNAN.YU
//Created date : 10/12/2007
//Version      : V1.2
//Last Modified: 19/08/2005
//Filesystem   : Read or Write MMC without any filesystem

//CopyRight (c) 2007 ZHENNAN.YU
//Email: yzn07@yahoo.com.cn
****************************************************************************************/

//------------------------------------------------------------
// Error define
//-------------------------------------------------------------
#define INIT_CMD0_ERROR     0x01
#define INIT_CMD1_ERROR		0x02
#define WRITE_BLOCK_ERROR	0x03
#define READ_BLOCK_ERROR   	0x04 
//-------------------------------------------------------------

// data type
//-------------------------------------------------------------   
// this structure holds info on the MMC card currently inserted 

typedef struct MMC_VOLUME_INFO
{ //MMC/SD Card info
  unsigned int  size_MB;
  unsigned char sector_multiply;
  unsigned int  sector_count;
  unsigned char name[6];
} VOLUME_INFO_TYPE; 

typedef struct STORE 
{ 
  unsigned char dat[100]; 
} BUFFER_TYPE; //256 bytes, 128 words

BUFFER_TYPE sectorBuffer; //512 bytes for sector buffer

//--------------------------------------------------------------
unsigned int  readPos=0;
unsigned char sectorPos=0;
unsigned char LBA_Opened=0; //Set to 1 when a sector is opened.
unsigned char Init_Flag;    //Set it to 1 when Init is processing.
//---------------------------------------------------------------

void MMC_Delay(unsigned int time);

//****************************************************************************
// Port Init
void MMC_Port_Init();
																			   
//****************************************************************************
//Routine for sending a byte to MMC/SD-Card
void Write_Byte_MMC(unsigned char value);


//****************************************************************************
//Routine for reading a byte from MMC/SD-Card
unsigned char Read_Byte_MMC();


//****************************************************************************
//Send a Command to MMC/SD-Card
//Return: the second byte of response register of MMC/SD-Card
unsigned char Write_Command_MMC(unsigned char *CMD);


//****************************************************************************
//Routine for Init MMC/SD card(SPI-MODE)
unsigned char MMC_Init();


//****************************************************************************
//Routine for reading data Registers of MMC/SD-Card
//Return 0 if no Error.
unsigned char MMC_Read_Block(unsigned char *CMD,unsigned char *Buffer,unsigned int Bytes);

//***************************************************************************
//Routine for reading CSD Registers from MMC/SD-Card (16Bytes)
//Return 0 if no Error.
unsigned char Read_CSD_MMC(unsigned char *Buffer);

//***************************************************************************
//Routine for reading CID Registers from MMC/SD-Card (16Bytes) 
//Return 0 if no Error.
unsigned char Read_CID_MMC(unsigned char *Buffer);

//****************************************************************************
//returns the :
// 	size of the card in MB ( ret * 1024^2) == bytes
// 	sector count and multiplier MB are in u08 == C_SIZE / (2^(9-C_SIZE_MULT))
// 	name of the media 
void MMC_get_volume_info(void);

//****************************************************************************
//Routine for writing a Block(512Byte) to MMC/SD-Card
//Return 0 if sector writing is completed.
unsigned char MMC_write_sector(unsigned long addr,unsigned char *Buffer);

//***************************************************************************
//Return: [0]-success or something error!
unsigned char MMC_Start_Read_Sector(unsigned long sector);

//***************************************************************************
void MMC_get_data(unsigned int Bytes,unsigned char *buffer);

//***************************************************************************
//��ȡ��������
void MMC_get_data_LBA(unsigned long lba, unsigned int Bytes,unsigned char *buffer);

//***************************************************************************
void MMC_LBA_Close();

//***************************************************************************
void MMC_GotoSectorOffset(unsigned long LBA,unsigned int offset);

#endif