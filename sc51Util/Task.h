#ifndef __TASK_H_
#define __TASK_H_
#include "..\sc51Core\Macros.h"
#define TASKSIZE 8
#define TASKMAXTICK 65535
typedef void(*taskfunc)();
typedef struct tagTaskNode
{
	taskfunc func;		//调度函数
	uint nexttick;		//下次调度时间 ms
	uint lentick;		//调度频率 ms 
}TaskNode;
extern TaskNode g_task_array[TASKSIZE];
extern uint g_task_tail;
extern uint g_task_tick;

/*----------------------------------------------------
 函数名：	TaskRegister()
 功能：		注册任务
 参数:		[in]func 任务函数
 			[in]lentick 调度频率 单位ms
 返回值:	结果
------------------------------------------------------*/
int TaskRegister(taskfunc func,uint lentick);

/*----------------------------------------------------
 函数名：	TaskUnRegister()
 功能：		注销任务
 参数:		[in]func 任务函数
 返回值:	无
------------------------------------------------------*/
int TaskUnRegister(taskfunc func);

/*----------------------------------------------------
 函数名：	TaskProc()
 功能：		任务处理的一次调用
 参数:		无
 返回值:	无
------------------------------------------------------*/
void TaskProc();

/*----------------------------------------------------
 函数名：	TaskClear()
 功能：		清空任务
 参数:		无
 返回值:	无
------------------------------------------------------*/
void TaskClear();

#endif