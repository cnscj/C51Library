#ifndef __EVENT_H_
#define __EVENT_H_
#include "..\sc51Core\Macros.h"
#define EVENTIDSIZE 16
extern uint g_eventid_array[EVENTIDSIZE];									//事件编号队列数组
extern uint g_eventid_front,g_eventid_tail;	//队列的首尾

#ifndef CLOSE_EVENTHANDLE

#define EVENTHANDLESIZE 8
typedef void(*eventfunc)();
typedef struct tagEventNode
{
	uint id;
	eventfunc func;
}EventNode;
extern EventNode g_eventhandle_array[EVENTHANDLESIZE];//用于保存处理函数
extern uint g_eventhandle_tail;
/*----------------------------------------------------
 函数名：	EventRegister()
 功能：		事件注册
 参数:		[in]id 事件编号
 			[in]func 事件函数
 返回值:	结果
------------------------------------------------------*/
int EventRegister(uint id,eventfunc func);

/*----------------------------------------------------
 函数名：	EventUnRegister()
 功能：		事件注销
 参数:		[in]func 事件函数
 返回值:	结果
------------------------------------------------------*/
int EventUnRegister(eventfunc func);

/*----------------------------------------------------
 函数名：	EventProc()
 功能：		事件处理
 参数:		无
 返回值:	无
------------------------------------------------------*/
void EventProc();

#endif
								
/*----------------------------------------------------
 函数名：	EventPush()
 功能：		入队事件
 参数:		[in]id 事件编号
 返回值:	结果
------------------------------------------------------*/
int EventPush(uint id);

/*----------------------------------------------------
 函数名：	EventPop()
 功能：		弹出事件
 参数:		无
 返回值:	结果
------------------------------------------------------*/
void EventPop();

/*----------------------------------------------------
 函数名：	EventGet()
 功能：		取得事件编号
 参数:		无
 返回值:	编号
------------------------------------------------------*/
uint EventGet();

/*----------------------------------------------------
 函数名：	EventIsEmpty()
 功能：		队列中的事件是否为空
 参数:		无
 返回值:	结果
------------------------------------------------------*/
int EventIsEmpty();


/*----------------------------------------------------
 函数名：	EventClear()
 功能：		清空事件队列
 参数:		无
 返回值:	无
------------------------------------------------------*/
void EventClear();

#endif