#ifndef __INTERRUPT_H_
#define __INTERRUPT_H_
#include "..\sc51Core\Macros.h"

#define INTERRUPT_TYPE(__TYPE__) interrupt __TYPE__ 								//设置中断类型
#define INTERRUPT_REG(__NUM__) using __NUM__										//中断寄存器	
#define INTERRUPT(__TYPE__,__NUM__) INTERRUPT_TYPE(__TYPE__) INTERRUPT_REG(__NUM__) //设置中断

#define INTERRUPT_FUNC(__TYPE__,__FUNC__)	\
void INTERRUPT_FUNCTION__TYPE__() INTERRUPT_TYPE(__TYPE__) \
{ \
__FUNC__; \
}

#define EX0_INTE 		INTERRUPT_TYPE(0)			//外部中断0
#define COUNTER0_INTE 	INTERRUPT_TYPE(1)			//计数器0中断
#define EX1_INTE 		INTERRUPT_TYPE(2)			//外部中断1
#define COUNTER1_INTE 	INTERRUPT_TYPE(3)			//计数器1中断
#define SERIAL_INTE 	INTERRUPT_TYPE(4)			//串口接收中断
#define COUNTER2_INTE 	INTERRUPT_TYPE(5)			//计数器2中断

#define TIMER0_INTE 	COUNTER0_INTE				//定时器0
#define TIMER1_INTE 	COUNTER1_INTE				//定时器1
#define TIMER2_INTE 	COUNTER2_INTE				//定时器2


#endif