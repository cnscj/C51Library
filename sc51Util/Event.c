#include "Event.h"

uint g_eventid_array[EVENTIDSIZE];									//事件编号队列数组
uint g_eventid_front=EVENTIDSIZE - 1,g_eventid_tail=EVENTIDSIZE - 1;	//队列的首尾

#ifndef CLOSE_EVENTHANDLE
EventNode g_eventhandle_array[EVENTHANDLESIZE];//用于保存处理函数
uint g_eventhandle_tail = 0;
/////////
int EventRegister(uint id,eventfunc func)
{
	if(g_eventhandle_tail >= EVENTHANDLESIZE) return 0;
	g_eventhandle_array[g_eventhandle_tail].id = id;
	g_eventhandle_array[g_eventhandle_tail].func = func;

	g_eventhandle_tail++;

	return 1;
}

int EventUnRegister(eventfunc func)
{
	uint i,j;
	for(i=0;i<g_eventhandle_tail;i++)
	{
		if(g_eventhandle_array[i].func == func)
		{
			for(j = i ;j < g_eventhandle_tail - 1;j++)
				g_eventhandle_array[j] = g_eventhandle_array[j + 1];

			g_eventhandle_tail--;
			return 1;
		}
	}
	return 0;
}


void EventProc()
{
	uint i;
	uint id;
	if(EventIsEmpty()) return ;
	
	id = EventGet();
	for(i = 0 ;i<g_eventhandle_tail;i++)
	{
		if(id == g_eventhandle_array[i].id)
			g_eventhandle_array[i].func();
	}
	EventPop();
}
#endif

int EventPush(uint id)
{
	if(g_eventid_front == (g_eventid_tail+1)%EVENTIDSIZE) return 0;
	else {
		g_eventid_tail=(g_eventid_tail+1)%EVENTIDSIZE;
		g_eventid_array[g_eventid_tail]=id;
		return 1;
	}
}

void EventPop()
{
	g_eventid_front = (g_eventid_front+1)%EVENTIDSIZE;
}

uint EventGet()
{
	uint id = g_eventid_array[(g_eventid_front+1)%EVENTIDSIZE];
	
	return id;
}

int EventIsEmpty()
{
	if(g_eventid_front==g_eventid_tail)	return 1;
	else return 0;
}

void EventClear()
{
	while(!EventIsEmpty())
		EventPop();
}