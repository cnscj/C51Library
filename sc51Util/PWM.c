#include "PWM.h"
#include <reg51.h>

uint g_pwm_speed=30;			//pwm速度
uint g_pwm_count=0;				//pwm计数器
pwmfunc g_pwm_func=NULL;
///////////////////
int PWMInit(double cycle,pwmfunc func)
{
	g_pwm_func = func;

	TMOD = 0x11;   //设置定时计数器工作方式1为定时器
	//TODO:计算值
	cycle = 0;

	//--定时器赋初始值，12MHZ下定时0.5ms--//
	//TODO:暂且采用0.5ms 
	#ifndef CLOSE_PWM_STEP
	TH0 = 0xFE; 
	TL0 = 0x0C;
	#endif

	TH1 = 0xFE; 
	TL1 = 0x0C;

	PWMStop();

	return 1;
}


void PWMStart(uint time)
{
	g_pwm_speed = time;
	g_pwm_count = 0;
	#ifndef CLOSE_PWM_STEP
	TR0 = 1;	 //开启定时器
	#endif
	TR1 = 1;	 //开启定时器
}

void PWMStep()
{
	if(g_pwm_count > g_pwm_speed)	//PWM周期为100*0.5ms
		g_pwm_count=0;
	
	g_pwm_func((g_pwm_count < g_pwm_speed));
}


void PWMStop()
{
	#ifndef CLOSE_PWM_STEP
	TR0 = 0;	 //关闭定时器
	#endif
	TR1 = 0;	 //关闭定时器
}

#ifndef CLOSE_PWM
#ifndef CLOSE_PWM_STEP
void PWM_TIMER0_INTERRUPT_FUNC() TIMER0_INTE
{	 
	PWMStep();							
}
#endif

void PWM_TIMER1_INTERRUPT_FUNC() TIMER1_INTE 
{
	g_pwm_count++;
}
#endif