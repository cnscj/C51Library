#include "Task.h"

TaskNode g_task_array[TASKSIZE];
uint g_task_tail = 0;
uint g_task_tick = 0;
//////////
int TaskRegister(taskfunc func,uint lentick)
{
	if(g_task_tail >= TASKSIZE) return 0;

	g_task_array[g_task_tail].func = func;
	g_task_array[g_task_tail].lentick = lentick;
	g_task_array[g_task_tail].nexttick = (g_task_tick + lentick)%TASKMAXTICK ;

	g_task_tail++;

	return 1;
}


int TaskUnRegister(taskfunc func)
{
	uint i,j;
	for(i=0;i<g_task_tail;i++)
	{
		if(g_task_array[i].func == func)
		{
			for(j = i ;j < g_task_tail - 1;j++)
				g_task_array[j] = g_task_array[j + 1];

			g_task_tail--;
			return 1;
		}
	}
	return 0;
	
}


void TaskProc()
{
	uint i;
	for(i=0;i<g_task_tail;i++)
	{
		if(g_task_array[i].nexttick >= g_task_tick)
		{
			g_task_array[i].func();
			g_task_array[i].nexttick = (g_task_tick + g_task_array[i].lentick)%TASKMAXTICK;	//TODO:��������?
		}
	}
	g_task_tick = (g_task_tick + 1)%TASKMAXTICK;
}


void TaskClear()
{
	g_task_tail = 0;
}