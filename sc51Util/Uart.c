#include "Uart.h"
#include "Calculate.h"
int UartInit(double cryfre,uint baud)
{
	//假设晶振频率为11.0592MHz，波特率为9600，SMOD=0，TH1初值应该为多少呢？根据上面公式可以计算得TH1=256-11059200/(9600*32*12)=0xFD。
	char th1,tl1;
	CalculateBaudRate(cryfre,baud,&th1,&tl1);
	return UartInitEx(0x20,0x50,th1,tl1,0x80);
}

int UartInitEx(char tmod,char scon,char th1,char tl1,char pcon)
{
	/*
	TMOD = 0x20;    //定时器T/C1工作方式2，定时器自动重载8位计数器
	SCON = 0x50;    //串口工作方式1，允许串口接收（SCON = 0x40 时禁止串口接收）
	TH1 = 0xFD; 	//定时器初值高8位设置（）
	TL1 = 0xFD; 	//定时器初值低8位设置
	PCON = 0x80;    //波特率倍频
	TR1 = 1;    	//定时器启动    
	*/

	TMOD = tmod;	//定时器T/C1工作方式2，定时器自动重载8位计数器
	SCON = scon;	//串口工作方式1，允许串口接收（SCON = 0x40 时禁止串口接收）
	TH1 = th1;		//定时器初值高8位设置（）
	TL1 = tl1;		//定时器初值低8位设置
	PCON = pcon;	//波特率倍频
}

int UartOpen()
{
	TR1 = 1;    	//定时器启动 
}

void UartPut(char ch)
{
	SBUF=ch;
	while(!TI);		//等待数据传输(TI表示没有数据要传,自动复位1表示传输完毕)
	TI=0;			//重置,表示还有数据需要传送
}


char UartGet()
{
	char ch=0;

	while(!RI);		//等待数据接收完成--会产出阻塞
	ch=SBUF; 		//出去接收到的数据
	RI = 0;
	
	return ch;
}

int UartSend(uchar *str,uint len)
{
	uint iCount;
	for(iCount=0;iCount<len;iCount++)
	{
		SBUF=str[iCount];
		while(!TI);					//等待数据传输(TI表示没有数据要传,自动复位1表示传输完毕)
		TI=0;						//重置,表示还有数据需要传送
	}

}

int UartReceive(uchar *buffer,uint size)
{
	uint i=0;
	while(1)
	{
		if(i>=size)
			   return i;
		else{
			if(RI){					//查看是否接收到数据(RI数据接收完成,自动复位1表示有数据接收)	
				buffer[i++]=SBUF; 	//出去接收到的数据
				//while(!RI);		//等待数据接收完成--会产出阻塞
				RI = 0;      		//清除接收中断标志位			
			}
			else{
		 		//TODO:超时返回2ms
				
					
				return i;
			}
		}
	}
}

int UartClose()
{
	TR1 = 0;    	//定时器关闭
}

//采用中断接收的方式-非阻塞
#define UARTRECV_FUNC(__FUNC__)	 \
void UARTRECV_INTERRUPT_FUNC() SERIAL_INTE \
{ \
	__FUNC__;\	
} 

