# Microsoft Developer Studio Project File - Name="SC51Lib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=SC51Lib - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SC51Lib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SC51Lib.mak" CFG="SC51Lib - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SC51Lib - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "SC51Lib - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SC51Lib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x804 /d "NDEBUG"
# ADD RSC /l 0x804 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "SC51Lib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../out"
# PROP Intermediate_Dir "../out"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x804 /d "_DEBUG"
# ADD RSC /l 0x804 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "SC51Lib - Win32 Release"
# Name "SC51Lib - Win32 Debug"
# Begin Group "sc51Util"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\sc51Util\Buffer.c
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Buffer.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Delayer.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Event.c
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Event.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Fat32.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\GPIO.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Interrupt.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\PWM.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\SPI.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Task.c
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Task.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Timer.h
# End Source File
# Begin Source File

SOURCE=.\sc51Util\Uart.h
# End Source File
# End Group
# Begin Group "sc51Core"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\sc51Core\List.c
# End Source File
# Begin Source File

SOURCE=.\sc51Core\List.h
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Macros.h
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Memory.c
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Memory.h
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Parse.c
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Parse.h
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Queue.c
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Queue.h
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Stack.c
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Stack.h
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Vector.c
# End Source File
# Begin Source File

SOURCE=.\sc51Core\Vector.h
# End Source File
# End Group
# Begin Group "sc51Drive"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\sc51Drive\ESP8266.h
# End Source File
# Begin Source File

SOURCE=.\sc51Drive\HC05.h
# End Source File
# Begin Source File

SOURCE=.\sc51Drive\LCD12864.h
# End Source File
# Begin Source File

SOURCE=.\sc51Drive\LCD1602.h
# End Source File
# Begin Source File

SOURCE=.\sc51Drive\SD.h
# End Source File
# Begin Source File

SOURCE=.\sc51Drive\UL2003.h
# End Source File
# End Group
# End Target
# End Project
