#ifndef __MACROS_H_
#define __MACROS_H_

typedef signed char         schar;
typedef unsigned char 		uchar;
typedef unsigned int 		uint;
typedef unsigned long		ulong;
typedef char* 				lpstr;
typedef schar				bool;

#define false 0
#define true 1
#define NULL 0

#define HIGH	1		 		//高电平
#define LOW		0				//低电平

#define DEFCRYFRE 11.0592		//默认晶振频率 MHZ
#define DEFCHIP "STC90C516RD" 	//默认芯片

#endif /*__MACROS_H_*/
