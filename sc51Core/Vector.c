#include "Vector.h"
#include "Memory.h"

int VectorInit(Vector *ptr,uint size)
{
	ptr->dat = (VECTORDATA *)Malloc(size);
	if(ptr->dat){
		ptr->maxsize = size;
		ptr->tail = 0;
		return 1;
	}
	else {
		ptr->maxsize = 0;
		ptr->tail = 0;
		return 0;
	}
}

int VectorPushBack(Vector *ptr,VECTORDATA dat)
{
	if(VectorIsFull(ptr)) return 0;
	ptr->dat[ptr->tail++]=dat;
	return 1;
}

int VectorPopBack(Vector *ptr)
{
	if(VectorIsEmpty(ptr)) return 0;
	ptr->tail--;
	return 1;
}

int VectorInsert(Vector *ptr,uint pos,VECTORDATA dat)
{
	uint iCount,nTotal;
	nTotal = VectorLength(ptr);
	if(nTotal + 1 >= ptr->maxsize || pos < 0) return 0;
	for(iCount = ptr->tail ;iCount >= pos ;iCount--)
	{
		ptr->dat[iCount] = ptr->dat[iCount -1];
	}
	ptr->dat[pos]=dat;
	ptr->tail++;
	return 1;
}

int VectorErase(Vector *ptr,uint pos)
{
	uint iCount,nTotal;
	nTotal = VectorLength(ptr);
	if(pos < 0 || pos >= nTotal) return 0;

	for(iCount = pos ;iCount < ptr->tail -1;iCount++)
	{
		ptr->dat[iCount] = ptr->dat[iCount + 1];
	}
	ptr->tail--;
	return 1;
}

int VectorGet(Vector *ptr,uint pos,VECTORDATA *ret)
{
	if(pos < 0) return 0;
	if(pos >= ptr->maxsize) return 0;
	if(!ret) return 0;

	*ret = ptr->dat[pos];
	return 1;
}

VECTORDATA VectorAt(Vector *ptr,uint pos)
{
	return ptr->dat[pos];
}

void VectorClear(Vector *ptr)
{
	ptr->tail = 0 ;
}


int VectorIsEmpty(Vector *ptr)
{
	return ptr->tail <= 0 ? 1:0;
}

int VectorIsFull(Vector *ptr)
{
	return ptr->tail >= ptr->maxsize ? 1:0;
}

uint VectorLength(Vector *ptr)
{
	return ptr->tail;
}