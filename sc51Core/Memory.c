#include "Memory.h"
#include <stdlib.h>
void *Malloc(uint size)
{
	return malloc(size);
}
void Free(void *ptr)
{
	free(ptr);
}