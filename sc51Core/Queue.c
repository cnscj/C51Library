#include "Queue.h"
#include "Memory.h"

int QueueInit(Queue *ptr)
{
	if(!ptr)return 0;

	ptr->front=ptr->back=NULL;
	ptr->length = 0;
	return 1;
}

int QueuePush(Queue *ptr,QUEUEDATA dat)
{
	QueueNode *node=(QueueNode *)Malloc(sizeof(QueueNode));
	node->dat = dat;
	node->next = NULL;
	ptr->back->next=node;
	ptr->back=node;

	ptr->length++;
	if(!ptr->front)ptr->front=node;

	return 1;
}

int QueuePop(Queue *ptr)
{
	QueueNode *next;
	if(QueueIsEmpty(ptr)) return 0;

	next = ptr->front->next;
	Free(ptr->front);
	ptr->front=next;

	ptr->length--;
	return 1;
}


QUEUEDATA QueueFront(Queue *ptr)
{
	return ptr->front->dat;
}

void QueueClear(Queue *ptr)
{
	while(!QueueIsEmpty(ptr))
		QueuePop(ptr);
	ptr->length=0;
}

int QueueIsEmpty(Queue *ptr)
{
	if(ptr->front == NULL && ptr->back == NULL) return 1;
	else return 0;
}

uint QueueLength(Queue *ptr)
{
	return ptr->length;
}