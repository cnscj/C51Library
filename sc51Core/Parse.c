#include "Parse.h"
#include <math.h>

char *DoubleToString(double dat,char *s,uchar jd)
{
	 int len,temp,flag=dat<0?dat=-dat,1:0,i;
	 char t[30];
	 temp=(int)dat;
	 for(len=0;temp>0;temp/=10,len++)
	 t[len]=temp%10+48;
	 for(i=0;i<=len;i++)
	 	s[len-i-1]=t[i];
	 s[len++]='.';
	 for(i=0,temp=(int)((dat-(int)dat)*pow(10,jd));temp>0;temp/=10,i++)
	 	t[i]=temp%10+48;
	 for(i=0;i<jd;i++)
	 	s[len++]=t[jd-i-1];
	 //s[len++]=0x0d;
	 //s[len++]=0x0a;
	 s[len]=0;
	 return s;
}

char *UCharToh16String(uchar datax, char *p)
{
	 uchar temp;
	 temp=datax;
	 p[0]=((temp>>4)>=10)?(temp>>4)+55:(temp>>4)+48;
	 p[1]=((temp&0x0f)>=10)?(temp&0x0f)+55:(temp&0x0f)+48;
	 p[2]=0;
 	 return p;
}

char *UCharToString(uchar num,char *pOut)
{
	uchar i;
	for(i=2;i>=0;i--){
		pOut[i]=num%10; 
		num/=10;
	}
	pOut[4]=0;
	return 	pOut;
}

/////////////////////////////////////////
uchar ReverseBit(uchar value)
{
	uchar answer = 0U;  
	uchar i;
    for (i = 1U; i != 0; i <<= 1)  
    {  
        answer <<= 1;  
        if (value & 1)  
        {  
            answer |= 1;  
        }  
        value >>= 1;  
    }  
    return answer;  
}

uchar CCCroL(uchar org, uchar bitNum)
{
	uchar i;  
    uchar high, low;  
    uchar after = org;  
      
    for (i = 0; i < bitNum; i++)  
    {  
        high = after & 0x80;  
        low = high >> 0x07;  
        after <<= 0x01;  
        after |= low;  
    }  
      
    return after;  
}  
  
 
uchar CCCroR(uchar org, uchar bitNum)
{
	uchar i;  
    uchar high, low;  
    uchar after = org;  
      
    for (i = 0; i < bitNum; i++)  
    {  
        low = after & 0x01;  
        high = low << 0x07;  
        after >>= 0x01;  
        after |= high;  
    }  
      
    return after;  
}