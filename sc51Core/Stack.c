#include "Stack.h"
#include "Memory.h"

int StackInit(Stack *ptr)
{
	if(!ptr) return 0;
	ptr->top = NULL;
	ptr->length = 0;
	return 1;
}

int StackPush(Stack *ptr,STACKDATA dat)
{
	StackNode *node = (StackNode *)Malloc(sizeof(StackNode));
	if(!node) return 0;

	node->dat=dat;
	node->next=ptr->top;
	ptr->top=node;
	ptr->length++;

	return 1;

}

int StackPop(Stack *ptr)
{
	StackNode *p=ptr->top;

	if(StackIsEmpty(ptr)) 
		return 0;
	else{
		ptr->top=ptr->top->next;
		Free(p);
		ptr->length--;
		return 1;
	}
}

STACKDATA StackTop(Stack *ptr)
{
	return ptr->top->dat;
}

void StackClear(Stack *ptr)
{
	while(!StackIsEmpty(ptr))
		StackPop(ptr);
	ptr->length=0;
}

int StackIsEmpty(Stack *ptr)
{
	if(ptr->top==NULL) return 1;
	else return 0;
}

uint StackLength(Stack *ptr)
{
	return ptr->length;
}