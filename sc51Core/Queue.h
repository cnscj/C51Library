#ifndef __QUEUE_H_
#define __QUEUE_H_
#include "Macros.h"
#include "Memory.h"
typedef char QUEUEDATA;
typedef struct tagQueueNode
{
	QUEUEDATA dat;
	struct tagQueueNode *next;
}QueueNode;
typedef struct tagQueue
{	
	struct tagQueueNode *front;
	struct tagQueueNode *back;
	uint length;
}Queue;

/*----------------------------------------------------
 函数名：	QueueInit()
 功能：		队列初始化
 参数:		[in]ptr 对象地址
 返回值:	结果
------------------------------------------------------*/
int QueueInit(Queue *ptr);

/*----------------------------------------------------
 函数名：	QueuePush()
 功能：		将数据入队
 参数:		[in]ptr 向量地址
 			[in]dat 数据
 返回值:	结果
------------------------------------------------------*/
int QueuePush(Queue *ptr,QUEUEDATA dat);

/*----------------------------------------------------
 函数名：	QueuePop()
 功能：		将数据出队
 参数:		[in]ptr 向量地址
 			[in]dat 数据
 返回值:	结果
------------------------------------------------------*/
int QueuePop(Queue *ptr);


/*----------------------------------------------------
 函数名：	QueueFront()
 功能：		取队头
 参数:		[in]ptr 对象地址
 返回值:	是否成功
------------------------------------------------------*/
QUEUEDATA QueueFront(Queue *ptr);


/*----------------------------------------------------
 函数名：	QueueClear()
 功能：		清空队列
 参数:		[in]ptr 对象地址
 返回值:	无
------------------------------------------------------*/
void QueueClear(Queue *ptr);

/*----------------------------------------------------
 函数名：	QueueIsEmpty()
 功能：		队列是否为空
 参数:		[in]ptr 对象地址
 返回值:	结果
------------------------------------------------------*/
int QueueIsEmpty(Queue *ptr);

/*----------------------------------------------------
 函数名：	QueueLength()
 功能：		取得队列长度
 参数:		[in]ptr 对象地址
 返回值:	队列长度
------------------------------------------------------*/
uint QueueLength(Queue *ptr);
#endif