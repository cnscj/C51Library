#include "List.h"
#include "Memory.h"

int ListInit(List *ptr)
{
	if(!ptr) return 0;
	ptr->tail = ptr->head = NULL;
	return 1;
}

int ListPushBack(List *ptr,LISTDATA dat)
{
	ListNode *temp = (ListNode *)Malloc(sizeof(ListNode));
	temp->dat = dat;
	temp->next = NULL;
	ptr->tail = temp;

	if(!ptr->head)ptr->head=temp;

	return 1;
}

int ListPushFront(List *ptr,LISTDATA dat)
{
	ListNode *temp = (ListNode *)Malloc(sizeof(ListNode));
	temp->dat = dat;
	temp->next = ptr->head;
	ptr->head = temp;

	if(!ptr->tail)ptr->tail=temp;

	return 1;
}

int ListPopBack(List *ptr)
{
	ListNode *last,*it;
	last=it=NULL;
	for(it=ptr->head; !it ;it=it->next)
		last = it;
	if(last)
	{
		last->next=ptr->tail->next;
		Free(ptr->tail);
		ptr->tail=last;

		return 1;
	}

	return 0;

}

int ListPopFront(List *ptr)
{
	ListNode *temp;
	if(ListIsEmpty(ptr)) return 0;

	temp = ptr->head->next;
	Free(ptr->head);
	ptr->head =	temp;

	return 1;

}

int ListInsert(List *ptr,uint pos,LISTDATA dat)
{
	ListNode **p=&ptr->head,*it=ptr->head;
	uint count=0;
	while(it)
	{
		if(count == pos)
		{
			ListNode *temp = (ListNode *)Malloc(sizeof(ListNode));
			temp->dat = dat;
			temp->next = *p;
			*p=temp;

			return 1;
		}
		p=&it->next;
		it=*p;
		count++;
	}
	return 0;
}

int ListErase(List *ptr,uint pos)
{
	ListNode *last=ptr->head,*it=ptr->head;
	uint count=0;
	while(it)
	{
		if(count == pos)
		{
			last->next=it->next;
			Free(it);

			return 1;
		}
		last=it;
		it=it->next;
		count++;
	}
	return 0;
}

int ListGet(List *ptr,uint pos,LISTDATA *ret)
{
	ListNode *it=ptr->head;
	uint i=0;
	if(!ret) return 0;

	while(it)
	{
		if(i == pos)
		{
			*ret=it->dat;
			return 1;
		}

		it=it->next;
		i++;
	}
	return 0;
}


LISTDATA ListAt(List *ptr,uint pos)
{
	ListNode *it=ptr->head;
	uint i=0;
	while(it)
	{
		if(i == pos)
			return it->dat;

		it=it->next;
		i++;
	}
	return 0;
}


void ListClear(List *ptr)
{
	ListNode *next,*it=ptr->head;
	while(it)
	{
		next=it->next;
		Free(next);
		it=next;
	}
}


int ListIsEmpty(List *ptr)
{
	if(ptr->tail == NULL && ptr->head ==NULL) return 1;
	else return 0;
}

uint ListLength(List *ptr)
{
	ListNode *it;
	uint count=0;
	for(it=ptr->head;!it;it=it->next)
		count++;
	return count;
}
