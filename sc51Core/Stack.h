#ifndef __STACK_H_
#define __STACK_H_
#include "Macros.h"
#include "Memory.h"

typedef char STACKDATA;
typedef struct tagStackNode
{
	STACKDATA dat;
	struct tagStackNode *next;
}StackNode;
typedef struct tagStack
{
	struct tagStackNode *top;
	uint length;
}Stack;

/*----------------------------------------------------
 函数名：	StackInit()
 功能：		栈初始化
 参数:		[in]ptr 对象地址
 返回值:	结果
------------------------------------------------------*/
int StackInit(Stack *ptr);

/*----------------------------------------------------
 函数名：	StackPush()
 功能：		将数据入栈
 参数:		[in]ptr 向量地址
 			[in]dat 数据
 返回值:	结果
------------------------------------------------------*/
int StackPush(Stack *ptr,STACKDATA dat);

/*----------------------------------------------------
 函数名：	StackPop()
 功能：		将数据出栈
 参数:		[in]ptr 向量地址
 			[in]dat 数据
 返回值:	结果
------------------------------------------------------*/
int StackPop(Stack *ptr);


/*----------------------------------------------------
 函数名：	StackTop()
 功能：		取栈顶
 参数:		[in]ptr 对象地址
 返回值:	数据
------------------------------------------------------*/
STACKDATA StackTop(Stack *ptr);


/*----------------------------------------------------
 函数名：	StackClear()
 功能：		清空栈
 参数:		[in]ptr 对象地址
 返回值:	无
------------------------------------------------------*/
void StackClear(Stack *ptr);

/*----------------------------------------------------
 函数名：	StackIsEmpty()
 功能：		栈是否为空
 参数:		[in]ptr 对象地址
 返回值:	结果
------------------------------------------------------*/
int StackIsEmpty(Stack *ptr);

/*----------------------------------------------------
 函数名：	StackLength()
 功能：		取得栈长度
 参数:		[in]ptr 对象地址
 返回值:	队列长度
------------------------------------------------------*/
uint StackLength(Stack *ptr);
#endif