#ifndef __ESP8266_H_
#define __ESP8266_H_
#include "..\sc51Util\Uart.h"

//定义引脚
#define ESP_TXD P3^0		//写端口
#define ESP_RXD P3^1		//读端口
/*----------------------------------------------------
 函数名：	ESPInit()
 功能：		初始化
 参数:		[in]baud 比特率
 返回值:	结果
------------------------------------------------------*/
int ESPInit(uint baud);

/*----------------------------------------------------
 函数名：	ESPCommand()
 功能：		发送AT指令,注:自动在末尾添加"\r\n"
 参数:		[in]cmd 命令
 			[out]ret 结果缓存区
 返回值:	结果
------------------------------------------------------*/
int ESPCommand(lpstr cmd,lpstr ret);

/*----------------------------------------------------
 函数名：	ESPConfig()
 功能：		模块配置
 参数:		[in]mode 模式
			[in]ssid SSID,可为NULL
			[in]psw SSID密码,可为NULl
 返回值:	结果
------------------------------------------------------*/
int ESPConfig(lpstr mode,lpstr ssid,lpstr psw);

/*----------------------------------------------------
 函数名：	ESPConnect()
 功能：		作为客户端连接
 参数:		[in]protocol 协议
			[in]port 端口
			[in]ip IP地址
 返回值:	结果
------------------------------------------------------*/
int ESPConnect(lptstr protocol,uint port,lpstr ip);

/*----------------------------------------------------
 函数名：	ESPListen()
 功能：		作为服务器监听
 参数:		[in]protocol 协议
			[in]port 端口
 返回值:	结果
------------------------------------------------------*/
int ESPListen(lptstr protocol,uint port);

/*----------------------------------------------------
 函数名：	ESPSend()
 功能：		发送消息给对方
 参数:		[in]num 客户编号,-1为广播
 			[in]str 发送的字符串
 返回值:	结果
------------------------------------------------------*/
int ESPSend(int num,lptstr str);

/*----------------------------------------------------
 函数名：	ESPReceive()
 功能：		接收对方消息
 参数:		[in]buf 缓存区
 			[in]size 缓存区大小
 返回值:	结果
------------------------------------------------------*/
int ESPReceive(lptstr buf,uint size);

/*----------------------------------------------------
 函数名：	ESPClose()
 功能：		关闭
 参数:		无
 返回值:	无
------------------------------------------------------*/
void ESPClose();
#endif