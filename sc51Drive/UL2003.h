#ifndef __UL2003_H_
#define __UL2003_H_
#include "..\sc51Util\Delayer.h"
//定义步进电机连接端口
sbit UL_A1=P1^0; 
sbit UL_B1=P1^1;
sbit UL_C1=P1^2;
sbit UL_D1=P1^3;
#define UL_COIL_AB1 {UL_A1=1;UL_B1=1;UL_C1=0;UL_D1=0;}//AB相通电，其他相断电
#define UL_COIL_BC1 {UL_A1=0;UL_B1=1;UL_C1=1;UL_D1=0;}//BC相通电，其他相断电
#define UL_COIL_CD1 {UL_A1=0;UL_B1=0;UL_C1=1;UL_D1=1;}//CD相通电，其他相断电
#define UL_COIL_DA1 {UL_A1=1;UL_B1=0;UL_C1=0;UL_D1=1;}//D相通电，其他相断电
#define UL_COIL_OFF {UL_A1=0;UL_B1=0;UL_C1=0;UL_D1=0;}//全部断电

/*----------------------------------------------------
 函数名：	ULStep()
 功能：		一次执行滴答
 参数:		[in]speed 转速
 			[in]dir 转向 (0顺,1逆)
 返回值:	无
------------------------------------------------------*/
void ULStep(uint speed,char dir);
 
#endif