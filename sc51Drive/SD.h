#ifndef __SD_H_
#define __SD_H_
#include "..\sc51Util\Fat32.h"
typedef struct tagSDFile
{
}SDFile;

char SDOpen(SDFile *file,char *path,char flag);
char SDIsOpen(SDFile *file);
uint SDWrite(SDFile *file,char *buf,uint len);

uint SDRead(SDFile *file,char *buf,uint size);
void SDClose(SDFile *file);
#endif   