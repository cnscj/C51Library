#ifndef __HC05_H_
#define __HC05_H_
#include "..\sc51Util\Uart.h"

//定义引脚
#define HC_TXD P3^0		//写端口
#define HC_RXD P3^1		//读端口

/*----------------------------------------------------
 函数名：	ESPInit()
 功能：		初始化
 参数:		[in]baud 比特率
 返回值:	结果
------------------------------------------------------*/
int HCInit(uint baud);

/*----------------------------------------------------
 函数名：	HCSend()
 功能：		发送字符串
 参数:		[in]str 字符串
 返回值:	结果
------------------------------------------------------*/
int HCSend(lptstr str);

/*----------------------------------------------------
 函数名：	HCReceive()
 功能：		接收字符串
 参数:		[in]baud 比特率
 返回值:	结果
------------------------------------------------------*/
int HCReceive(lpstr buf,uint len);

/*----------------------------------------------------
 函数名：	HCClose()
 功能：		关闭
 参数:		无
 返回值:	无
------------------------------------------------------*/
void HCClose();


#endif